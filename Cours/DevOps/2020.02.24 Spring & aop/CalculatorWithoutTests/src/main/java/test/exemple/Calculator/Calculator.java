package test.exemple.Calculator;

public class Calculator implements ICalculator {

	@Override
	public int add(int x, int y) { return x+y; }

	@Override
	public int substract(int x ,int y) { return x-y; }

	@Override
	public int multiply(int x, int y) { return x*y; }

	@Override
	public int divide(int x, int y) { return x/y; }
}