package test.exemple.Calculator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MathParser {

	@Autowired
	ICalculator calculator;
	
//	public MathParser(ICalculator calculator) {
//		this.calculator = calculator;
//	}

	public int parse(String input) throws Exception {
		return parse(new StringBuffer(input));
	}
	
	/**
	 * @param input
	 * @return
	 * @throws Exception
	 */
	public int parse(StringBuffer input) throws Exception {
		int x, y;
		char operator;
		// parsing x
		x = parseInt(input);
		// parsing operator
		operator = parseOperator(input);
		// parsing y
		y = parseInt(input);
		// computing and returning result
		switch (operator) {
		case '+' :
			return calculator.add(x, y);
		case '-' :
			return calculator.substract(x, y);
		case '*' :
			return calculator.multiply(x, y);
		case '/' :
			return calculator.divide(x, y);
		default :
			throw new Exception("operation mal formée");
		}
	}
	
	private void skipWhiteSpaces(StringBuffer input) {
		int pos = 0;
		while (input.length() > pos && input.charAt(pos) == ' ') pos++;
		input.delete(0, pos);
	}
	
	private int parseInt(StringBuffer input) {
		skipWhiteSpaces(input);
		int pos = 0;
		while (input.length() > pos && input.charAt(pos) >= '0' && input.charAt(pos) <= '9') pos++;
		int n = Integer.parseInt(input.substring(0, pos));
		input.delete(0,  pos);
		return n;
	}
	
	private char parseOperator(StringBuffer input) throws Exception {
		skipWhiteSpaces(input);
		char op = input.charAt(0);
		if (op != '+' && op != '-' && op != '*' && op != '/')
			throw new Exception("operation mal formée");
		input.delete(0,  1);
		return op;
	}	
}
