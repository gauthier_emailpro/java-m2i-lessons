package test.exemple;

import java.util.Scanner;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import test.exemple.Calculator.MathParser;
import test.exemple.config.ApplicationConfig;

public class Main {

	public static void main(String[] args) throws Exception {
		try (AnnotationConfigApplicationContext acac = new AnnotationConfigApplicationContext(
				ApplicationConfig.class)) {
			Scanner scan = new Scanner(System.in);
			MathParser mp = (MathParser) acac.getBean(MathParser.class);
			int x = 8;
			int y = 0;
			String input = x + " + " + y;
			System.out.println("Addition : " + x + " + " + y + " =");
			System.out.println(mp.parse(input));

			System.out.println("division : " + x + " / " + y + " =");
			input = x + " / " + y;
			try {
				System.out.println(mp.parse(input));
			} catch (ArithmeticException e) {
				System.out.println("division par O impossible");
			}
			y = 4;
			input = x + " / " + y;
			System.out.println(mp.parse(input));
			
			
		}

	}

}

//acac.refresh();
//MemberService ms = (MemberService) acac.getBean(MemberService.class);
//MemberService ms = (MemberService) acac.getBean("memberServiceRenamed");
//System.out.println(ms.findAll());
