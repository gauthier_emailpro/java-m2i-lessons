package test.exemple.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LogAspect {

	@Pointcut("execution(* test.exemple.Calculator.Calculator.add(..))")
	public void calculatorAdd() {};

	@Pointcut("execution(* test.exemple.Calculator.Calculator.divide(..))")
	public void calculatorDivide() {};
	
	@Before("calculatorAdd()")
	public void beforeAdd() {
		System.out.println("Before add !");
	}

	@After("calculatorAdd()")
	public void afterAdd() {
		System.out.println("after add !");
	}

	@AfterReturning(pointcut = "calculatorAdd()", returning = "retValue")
	public void afterReturningAdd(Object retValue) {
		System.out.println("afterReturning : ret value = " + retValue);
	}

	@AfterThrowing(pointcut = "calculatorDivide()", throwing = "exc")
	public void afterThrowingDivide(ArithmeticException exc) {
		System.out.println("Exception from aspect = "  + exc);
	}

	@Around("calculatorDivide()")
	public Object arroudDivide(ProceedingJoinPoint joinpoint) throws Throwable {
		System.out.println("getTarget " + joinpoint.getTarget());
		System.out.println("getThis " + joinpoint.getThis());
		System.out.println("getSignature " + joinpoint.getSignature());
		System.out.println("getArgs " + joinpoint.getArgs());
		System.out.println("Avant exécution de la méthode");
		System.out.println(joinpoint);
		Object returnValue = joinpoint.proceed();
		System.out.println("Après exécution de la méthode");
		return returnValue;
	}
	
	@Before("@annotation(aspects.Log)")
	public void logAnnotation() {
		System.out.println("log annotation");
	}
}
