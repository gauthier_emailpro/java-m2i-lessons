package test.exemple.Calculator;

import org.springframework.stereotype.Component;

/**
 * Interface pour les calculator. Définit les opérations de base devant etre supportées. 
 * @author andre
 *
 */
@Component
public interface ICalculator {
	/**
	 * Réalise des additions.
	 * @param x un nombre
	 * @param y un autre nombre
	 * @return le resultat de l'addition de x et y
	 */
	public int add(int x, int y);
	
	public int substract(int x ,int y);
	
	public int multiply(int x, int y);
	
	public int divide(int x, int y);
}
