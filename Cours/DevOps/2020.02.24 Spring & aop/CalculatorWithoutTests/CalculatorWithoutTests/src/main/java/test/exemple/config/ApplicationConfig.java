package test.exemple.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan({"test.exemple.Calculator", "test.exemple.aspects"})
@EnableAspectJAutoProxy
public class ApplicationConfig {

}
