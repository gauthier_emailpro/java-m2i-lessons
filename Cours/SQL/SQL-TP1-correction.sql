
-- Exercice 1 -------------------------------------------------------------------------------------

drop database if exists Tp1Exercice1;
create database Tp1Exercice1;

use Tp1Exercice1;

create table Client (
    N_Client INT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(255),
    adresse VARCHAR(255),
    tel VARCHAR(255)
);

create table Commande (
    numCo INT PRIMARY KEY AUTO_INCREMENT,
    date DATETIME,
    numCl INT,
    CONSTRAINT FK_Client FOREIGN KEY (NumCl) REFERENCES Client(N_Client)
);

create table Produit (
	numPr INT PRIMARY KEY AUTO_INCREMENT, 
	nom VARCHAR(255), 
	prix DECIMAL
);

create table Constitution (
	numCo INT, 
	numP INT, 
	quantite INT, 
	CONSTRAINT FK_Commande FOREIGN KEY (NumCo) REFERENCES Commande(numCo), 
	CONSTRAINT FK_Produit FOREIGN KEY (NumP) REFERENCES Produit(numPr),
	CONSTRAINT PK_Constitution PRIMARY KEY (numCo, numP)
);

show tables;
desc Client;
desc Commande;
desc Produit;
desc Constitution;


-- Exercice 2 -------------------------------------------------------------------------------------

drop database if exists Tp1Exercice2;
create database Tp1Exercice2;

use Tp1Exercice2;

create table Client (
    num INT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(255),
    adresse VARCHAR(255),
    tel VARCHAR(255)
);

create table DemandeReservation (
    num INT PRIMARY KEY AUTO_INCREMENT,
    dateDebut DATE,
    dateFin DATE,
	region VARCHAR(255),
	numClient INT,
    CONSTRAINT FK_DemandeReservation_Client FOREIGN KEY (numClient) REFERENCES Client(num)
);

create table TypeChambre (
	code INT PRIMARY KEY AUTO_INCREMENT, 
	nombrePersonne INT, 
	commodites VARCHAR(255)
);

create table Constitution (
	numDemandeReservation INT, 
	codeTypeChambre INT, 
	nombreChambre INT, 
	CONSTRAINT FK_Constitution_DemandeReservation FOREIGN KEY (numDemandeReservation) REFERENCES DemandeReservation(num), 
	CONSTRAINT FK_Constitution_TypeChambre FOREIGN KEY (codeTypeChambre) REFERENCES TypeChambre(code),
	CONSTRAINT PK_Constitution PRIMARY KEY (numDemandeReservation, codeTypeChambre)
);

show tables;
desc Client;
desc DemandeReservation;
desc TypeChambre;
desc Constitution;


-- Exercice 3 ----------------------------------------------------------------------------------------

drop database if exists Tp1Exercice3;
create database Tp1Exercice3;

use Tp1Exercice3;

create table Cours (
    code INT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(255),
    description VARCHAR(255),
    ect INT
);

create table Salle (
    code INT PRIMARY KEY AUTO_INCREMENT,
	localisation VARCHAR(255),
    discrimation VARCHAR(32),
    nombrePlaces INT,
    nombreOrdinateurs INT
);

create table Occupation (
	codeCours INT, 
	codeSalle INT, 
	date DATE, 
	heure TIME,
	duree TIME,
	CONSTRAINT FK_Occupation_Cours FOREIGN KEY (codeCours) REFERENCES Cours(code), 
	CONSTRAINT FK_Occupation_Salle FOREIGN KEY (codeSalle) REFERENCES Salle(code),
	CONSTRAINT PK_Occupation PRIMARY KEY (codeCours, codeSalle)
);

show tables;
desc Cours;
desc Salle;
desc Occupation;




-- Exercice 4a (SINGLE TABLE) ---------------------------------------------------------------------

drop database if exists Tp1Exercice4a;
create database Tp1Exercice4a;

use Tp1Exercice4a;

create table Position (
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(255),
    salaryMin INT,
    salaryMax INT
);

create table Task (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    description VARCHAR(255)
);

create table TaskForce (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
	idTask INT,
	CONSTRAINT FK_TaskForce_Task FOREIGN KEY (idTask) REFERENCES Task(id)
);

create table Employee (
    id INT PRIMARY KEY AUTO_INCREMENT,
    discrimation VARCHAR(32),
	name VARCHAR(255),
	startDate DATE,
	hourlySalary INT,
	monthlySalary INT,
	idPosition INT,
	idTaskForce INT, 
	CONSTRAINT FK_Employee_TaskForce FOREIGN KEY (idTaskForce) REFERENCES TaskForce(id),
	CONSTRAINT FK_Employee_Position FOREIGN KEY (idPosition) REFERENCES Position (id)
);

create table Employee_Task (
	idEmployee INT,
	idTask INT,
	CONSTRAINT FK_Employee_Task_Employee FOREIGN KEY (idEmployee) REFERENCES Employee(id), 
	CONSTRAINT FK_Employee_Task_Task FOREIGN KEY (idTask) REFERENCES Task(id),
	CONSTRAINT PK_Employee_Task PRIMARY KEY (idEmployee, idTask)
);


-- Exercice 4b (JOIN) -----------------------------------------------------------------------------

drop database if exists Tp1Exercice4b;
create database Tp1Exercice4b;

use Tp1Exercice4b;

create table Position (
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(255),
    salaryMin INT,
    salaryMax INT
);

create table Task (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    description VARCHAR(255)
);

create table TaskForce (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
	idTask INT,
	CONSTRAINT FK_TaskForce_Task FOREIGN KEY (idTask) REFERENCES Task(id)
);

create table Employee (
    id INT PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255),
	startDate DATE,
	idPosition INT,
	idTaskForce INT, 
	CONSTRAINT FK_Employee_TaskForce FOREIGN KEY (idTaskForce) REFERENCES TaskForce(id),
	CONSTRAINT FK_Employee_Position FOREIGN KEY (idPosition) REFERENCES Position (id)
);

create table Operative (
	idEmployee INT PRIMARY KEY,
	hourlySalary INT,
	CONSTRAINT FK_Operative_Employee FOREIGN KEY (idEmployee) REFERENCES Employee(id)
);

create table Manager (
	idEmployee INT PRIMARY KEY,
	monthlySalary INT,
	CONSTRAINT FK_Manager_Employee FOREIGN KEY (idEmployee) REFERENCES Employee(id)
);

create table Employee_Task (
	idEmployee INT,
	idTask INT,
	CONSTRAINT FK_Employee_Task_Employee FOREIGN KEY (idEmployee) REFERENCES Employee(id), 
	CONSTRAINT FK_Employee_Task_Task FOREIGN KEY (idTask) REFERENCES Task(id),
	CONSTRAINT PK_Employee_Task PRIMARY KEY (idEmployee, idTask)
);


-- Exercice 4c (TABLE PER CLASS) ------------------------------------------------------------------

drop database if exists Tp1Exercice4c;
create database Tp1Exercice4c;

use Tp1Exercice4c;

create table Position (
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(255),
    salaryMin INT,
    salaryMax INT
);

create table Task (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    description VARCHAR(255)
);

create table TaskForce (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
	idTask INT,
	CONSTRAINT FK_TaskForce_Task FOREIGN KEY (idTask) REFERENCES Task(id)
);

create table Employee (
    id INT PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255),
	startDate DATE,
	idPosition INT,
	idTaskForce INT, 
	CONSTRAINT FK_Employee_TaskForce FOREIGN KEY (idTaskForce) REFERENCES TaskForce(id),
	CONSTRAINT FK_Employee_Position FOREIGN KEY (idPosition) REFERENCES Position (id)
);

create table Operative (
	idEmployee INT PRIMARY KEY,
	name VARCHAR(255),
	startDate DATE,
	idPosition INT,
	idTaskForce INT, 
	hourlySalary INT,
	CONSTRAINT FK_Operative_Employee FOREIGN KEY (idEmployee) REFERENCES Employee(id),
	CONSTRAINT FK_Operative_TaskForce FOREIGN KEY (idTaskForce) REFERENCES TaskForce(id),
	CONSTRAINT FK_Operative_Position FOREIGN KEY (idPosition) REFERENCES Position (id)
);

create table Manager (
	idEmployee INT PRIMARY KEY,
	name VARCHAR(255),
	startDate DATE,
	idPosition INT,
	idTaskForce INT, 
	monthlySalary INT,
	CONSTRAINT FK_Manager_Employee FOREIGN KEY (idEmployee) REFERENCES Employee(id),
	CONSTRAINT FK_Manager_TaskForce FOREIGN KEY (idTaskForce) REFERENCES TaskForce(id),
	CONSTRAINT FK_Manager_Position FOREIGN KEY (idPosition) REFERENCES Position (id) 
);

create table Employee_Task (
	idEmployee INT,
	idTask INT,
	CONSTRAINT FK_Employee_Task_Employee FOREIGN KEY (idEmployee) REFERENCES Employee(id), 
	CONSTRAINT FK_Employee_Task_Task FOREIGN KEY (idTask) REFERENCES Task(id),
	CONSTRAINT PK_Employee_Task PRIMARY KEY (idEmployee, idTask)
);