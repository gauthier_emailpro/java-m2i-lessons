use LOCATION;

-- Exercice 1

-- 1
\! echo "\n> Exercice 1.1 ----------------------------------------------------\n"
\! echo "Pour chaque habitation, afficher son code, son type, la ville où elle se trouve les noms des locataires et leur profession."

-- avec la syntaxe courte des inner joins
\! echo "\n\t- avec la syntaxe courte des inner join :\n"
select H.Codeh, H.Typeh, H.Ville, C.Nom, C.Profession 
	from HABITATION H, LOCATION L, CLIENT C 
	where L.codeh = H.Codeh and L.Nom = C.Nom
	order by H.codeh;

-- ou avec la syntaxe complète
\! echo "\n	- avec la syntaxe longue des inner join :\n"
select H.Codeh, H.Typeh, H.Ville, C.Nom, C.Profession 
	from HABITATION H 
	inner join LOCATION L on H.Codeh = L.Codeh 
	inner join CLIENT C on L.Nom = C.Nom;



-- 2
\! echo "\nExercice 1.2 ----------------------------------------------------\n"
\! echo "La même que la requête précédente mais afficher aussi les habitations qui n’ont jamais été prises en location"

-- avec une union
\! echo "\n\t- avec un UNION :\n"
select H.Codeh, H.Typeh, H.Ville, C.Nom, C.Profession 
	from HABITATION H 
	inner join LOCATION L on H.Codeh = L.Codeh 
	inner join CLIENT C on L.Nom = C.Nom 
union 
select H.Codeh, H.Typeh, H.Ville, null, null
	from HABITATION H 
	left outer join LOCATION L on H.Codeh = L.Codeh 
	where L.Codeh is null;

-- ou avec un left outer join
\! echo "\n\t- avec LEFT OUTER JOIN :\n"
SELECT *
	FROM HABITATION h 
	LEFT OUTER JOIN LOCATION l ON h.codeh = l.codeh
	LEFT OUTER JOIN CLIENT c ON l.nom = c.nom;

-- ou avec un right outer join
\! echo "\n\t- avec RIGHT OUTER JOIN :\n"
select * 
	from CLIENT c 
	inner join LOCATION l on c.nom = l.nom 
	right outer join HABITATION h on l.codeh = h.codeh;



-- 3
\! echo "\nExercice 1.3 ----------------------------------------------------\n"
\! echo "Déterminer le loyer minimum, maximum et moyen des habitations pour chaque type et chaque ville (dans une seule requête)"

-- par type ET ville
\! echo "\n\t- par type ET ville :\n"
select TypeH, Ville, count(*), min(LoyerM), max(LoyerM), avg(LoyerM) 
	from HABITATION 
	group by Typeh, Ville
	order by Ville;

-- ou par type PUIS ville
\! echo "\n\t- par type, puis par ville :\n"
select TypeH as "Type/Ville", count(*), min(LoyerM), max(LoyerM), avg(LoyerM) 
	from HABITATION 
	group by Typeh
union
select Ville as "Type/Ville", count(*), min(LoyerM), max(LoyerM), avg(LoyerM) 
	from HABITATION 
	group by Ville;



-- 4
\! echo "\nExercice 1.4 ----------------------------------------------------\n"
\! echo "Pour chaque type d’habitation, déterminer le nombre d’habitations de ce type qui ont été prises en location."

-- avec un count distinct
select H.TypeH, count(distinct H.Codeh)
	from HABITATION H 
	inner join LOCATION L on H.Codeh = L.Codeh 
	group by Typeh
	order by TypeH;

-- avec un sous-select
select H.TypeH, count(*) 
	from HABITATION H
	where exists (select * from LOCATION L where L.Codeh = H.Codeh)
	group by Typeh
	order by TypeH;



-- 5
\! echo "\nExercice 1.5 ----------------------------------------------------\n"
\! echo "Même que la requête précédente, mais montrer seulement les types dont au moins 3 habitations (pas forcement différentes) ont été louées."

select H.TypeH, count(distinct H.Codeh)
	from HABITATION H 
	inner join LOCATION L on H.Codeh = L.Codeh 
	group by Typeh 
		having count(H.Codeh) >= 3;



-- 6
\! echo "\nExercice 1.6 ----------------------------------------------------\n"
\! echo "Pour chaque habitation déterminer le nombre total des mois qui a été louée. Afficher code et type. Facultatif : pour celles qui n’ont été jamais louées, afficher la valeur 0 (utiliser la fonction isnull(valeurCalculé, 0))."

-- version basique
\! echo "\n\t- version basique (que celles qui ont été louées) :\n"
select H.Codeh, sum(L.NombMois) 
	from HABITATION H 
	inner join LOCATION L on H.Codeh = L.Codeh 
	group by H.Codeh;

-- amelioration
\! echo "\n\t- version améliorée (avec les habitations jamais louées) :\n"
select H.Codeh, IFNULL(sum(L.NombMois), 0)
	from HABITATION H 
	left outer join LOCATION L on H.Codeh = L.Codeh 
	group by H.Codeh;



-- 7
\! echo "\nExercice 1.7 ----------------------------------------------------\n"
\! echo "Pour chaque client calculer ses frais totaux de loyer."

select C.Nom, sum(H.LoyerM*L.NombMois) 
	from HABITATION H 
	inner join LOCATION L on H.Codeh = L.Codeh 
	inner join CLIENT C on C.Nom = L.Nom 
	group by C.Nom;



-- 8
\! echo "\nExercice 1.8 ----------------------------------------------------\n"
\! echo "Trouver les clients qui n’ont jamais loué aucune habitation."
-- avec in right outer join
select C.Nom 
	from LOCATION L 
	right outer join CLIENT C on C.Nom = L.Nom 
	where L.nom is null;
-- avec un sous-select
select C.Nom 
	from CLIENT C
	where not exists (select * from LOCATION L where L.nom=C.nom);



-- 9
\! echo "\nExercice 1.9 ----------------------------------------------------\n"
\! echo "Trouver les clients qui ont loué à la fois des appartements de type 1 et de type 3, c’est-à dire qui ont loué (au moins) un appartement de type 1 et (au moins) un appartement de type 3. (Utiliser exists)."

-- avec 2 sous-select
\! echo "\n\t- avec 2 sous-select :\n"
select C.nom 
	from CLIENT C 
	where exists (
		select * 
		from HABITATION H , LOCATION L
		where H.Codeh = L.Codeh and L.nom = C.nom and H.Typeh='Type1')
	and exists (
		select * 
		from HABITATION H, LOCATION L
		where H.Codeh = L.Codeh and L.nom = C.nom and H.Typeh='Type3');

-- ou avec un sous-select et une jointure
\! echo "\n\t- avec un sous-select et une jointure :\n"
select C.nom 
	from CLIENT C, HABITATION H , LOCATION L
	where H.Codeh = L.Codeh and L.nom = C.nom and H.Typeh='Type1'
	and exists (
		select * 
		from HABITATION H, LOCATION L
		where H.Codeh = L.Codeh and L.nom = C.nom and H.Typeh='Type3');

-- ou avec un grosse jointure
\! echo "\n\t- avec une (grosse) jointure :\n"
select distinct C.nom 
	from CLIENT C, HABITATION H1 , LOCATION L1, HABITATION H2 , LOCATION L2
	where H1.Codeh = L1.Codeh and L1.nom = C.nom and H1.typeh='Type3'
	and H2.Codeh = L2.Codeh and L2.nom = C.nom and H2.Typeh='Type1';



-- 10
\! echo "\nExercice 1.10 ----------------------------------------------------\n"
\! echo "Trouver les clients qui n’ont loué que des villas (toutes les habitations qui ont louées sont des villas). Le résultat ne doit pas afficher les clients qui n’ont pas loué aucune habitation."

-- avec une jointure et un sous-select
\! echo "\n\t- avec un sous-select et une jointure :\n"
select c.nom
	from CLIENT c
	inner join LOCATION l on c.nom = l.nom
	inner join HABITATION h on l.codeh = h.codeh
	where h.typeh="Villa"
	and not exists (select c.nom
		from LOCATION l
		inner join HABITATION h on l.codeh = h.codeh
		where c.nom = l.nom and h.typeh!="Villa");

-- ou avec 2 sous-select
\! echo "\n\t- avec 2 sous-select :\n"
select C.nom 
	from CLIENT C 
	where 'Villa' = ALL (select H.Typeh 
				from HABITATION H 
				inner join LOCATION L on H.Codeh = L.Codeh 
				where C.Nom = L.Nom)
	and exists (select H.Typeh 
				from HABITATION H 
				inner join LOCATION L on H.Codeh = L.Codeh 
				where C.Nom = L.Nom);



-- 11
\! echo "\nExercice 1.11 ----------------------------------------------------\n"
\! echo "Trouver les clients qui ont dépensé le maximum en loyer (afficher Nom de client et montant)."

select C.nom, sum(H.LoyerM*L.NombMois) 
	from CLIENT C 
	inner join LOCATION L on C.Nom = L.Nom 
	inner join HABITATION H on H.Codeh = L.Codeh 
	group by C.nom 
	having sum(H.LoyerM*L.NombMois) >= ALL (
		select sum(H.LoyerM*L.NombMois) 
			from CLIENT C 
			inner join LOCATION L on C.Nom = L.Nom 
			inner join HABITATION H on H.Codeh = L.Codeh 
		group by C.nom);


-- Exercice 2

-- 1
\! echo "\nExercice 2.1 ----------------------------------------------------\n"
\! echo "Répondez à la question 7 de l’exercice 1 de TP 2 en utilisant les vues."
drop view if exists ClientEtLoyers;
create view ClientEtLoyers (nom, sommeLoyers) as
	select C.Nom, sum(H.LoyerM*L.NombMois)
		from HABITATION H  
		inner join LOCATION L on H.Codeh = L.Codeh 
		inner join CLIENT C on C.Nom = L.Nom 
		group by C.Nom;

-- 2
\! echo "\nExercice 2.2 ----------------------------------------------------\n"
\! echo "La personne qui a le maximum des frais de loyer (avec les vues)."
select * 
	from ClientEtLoyers C 
	where C.sommeLoyers = (
		select max(sommeLoyers) 
		from ClientEtLoyers);

-- 3
\! echo "\nExercice 2.3 ----------------------------------------------------\n"
\! echo "Créez une vue marseille contenant uniquement les habitations de la ville de Marseille."
drop view if exists Marseille;
create view Marseille as select * from HABITATION H where H.Ville = 'Marseille';

-- 4
\! echo "\nExercice 2.4 ----------------------------------------------------\n"
\! echo "Ajoutez une nouvelle habitation à la vue marseille ayant les attributs suivants : (950, ’TYPE3’, ’St Marguerite 6’, 1000.00)."
delete from HABITATION where CodeH=950;
insert into Marseille (CodeH, TypeH, Adresse, LoyerM) values (950, 'TYPE3', 'St Marguerite 6', 1000.00);

-- 5
\! echo "\nExercice 2.5 ----------------------------------------------------\n"
\! echo "Vérifiez que l’habitation ayant comme code 950 a été ajoutée à la table habitation. Que remarquez vous ?"
select * from HABITATION;
\! echo "La colonne Ville est NULL."

-- 6
\! echo "\nExercice 2.6 ----------------------------------------------------\n"
\! echo "Comment peut-on résoudre ce genre du problème ?"
\! echo "Plusieurs solutions :"
\! echo "\t- ajouter la ville a la requete d'insertion"
\! echo "\t- définir une valeur par défaut pour la colonne Ville dans la table habitation"
\! echo "\t- interdir l'insertion :"
\! echo "\t\t- ajouter une contrainte not null sur la colonne Ville de la table habitation"
\! echo "\t\t- ajouter le CHECK OPTION sur la vue"

-- 7
\! echo "\nExercice 2.7 ----------------------------------------------------\n"
\! echo "Créez une vue loc marseille contenant les locations des Habitations de Marseille."
drop view if exists Loc_Marseille;
create view Loc_Marseille as 
	select L.* 
	from LOCATION L 
	inner join HABITATION H on L.Codeh = H.Codeh 
	where H.ville = 'Marseille';

-- 8
\! echo "\nExercice 2.8 ----------------------------------------------------\n"
\! echo "Insérez dans loc marseille les attributs suivants (964, ’Siegel’, 6), que remarquez vous ?"
delete from LOCATION where CodeH=964 and Nom='Siegel' and NombMois=6;
insert into Loc_Marseille (CodeH, Nom, NombMois) values (964, 'Siegel', 6);
\! echo "Marche ! L'insertion concerne les colonnes d'une seule table, pas de problème."
\! echo "Par contre, le n-uplet n'apparait pas dans la vue Loc_Marseille. On peut empecher l'insertion avec CHECK OPTION."