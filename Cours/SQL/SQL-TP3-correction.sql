use FILM;

-- Exercice 1

-- 1
\! echo "\n> Exercice 1.1 ----------------------------------------------------\n"
\! echo "Le nombre d’épisode total de toutes les saisons de la série ’Navarro’."
select count(*) 
	from SERIE Se 
	inner join SAISON Sa on Se.idserie = Sa.idserie 
	inner join EPISODE E on Sa.idsaison = E.idsaison and Se.idserie = E.idserie
	where Se.titre = 'Navarro';

-- 2
\! echo "\n> Exercice 1.2 ----------------------------------------------------\n"
\! echo "La saison ayant le plus grand nombre d’épisodes dans la série ’Navarro’."
select Sa.idsaison, count(E.numepisode) 
	from SAISON Sa 
	inner join SERIE Se on Sa.idserie = Se.idserie 
	inner join EPISODE E on E.idsaison = Sa.idsaison 
	where Se.titre = 'Navarro' 
	group by Sa.idsaison 
	having count(E.numepisode) >= ALL  (
		select count(E.numepisode) 
		from SAISON Sa 
			inner join SERIE Se on Sa.idserie = Se.idserie 
			inner join EPISODE E on E.idsaison = Sa.idsaison 
		where Se.titre = 'Navarro' 
		group by Sa.idsaison);

-- 2 bis
create or replace view EpisodeParSaisonDeNavarro (idSaison, nbEpisode) as
	select Sa.idsaison, count(E.numepisode) 
	from SAISON Sa 
	inner join SERIE Se on Sa.idserie = Se.idserie 
	inner join EPISODE E on E.idsaison = Sa.idsaison 
	where Se.titre = 'Navarro' 
	group by Sa.idsaison;

select *
	from EpisodeParSaisonDeNavarro
	where nbEpisode >= ALL (
		select nbEpisode
		from EpisodeParSaisonDeNavarro);


-- 3
\! echo "\n> Exercice 1.3 ----------------------------------------------------\n"
\! echo "Le nom des acteurs qui ont participé à la saison 1 de ’Navarro’ mais pas à la saison 10."
-- avec 2 sous-select
select distinct A.nom 
	from ACTEUR A 
	where '1' = ANY(
		select Sa.idsaison 
			from PARTICIPATION P 
			inner join SERIE Se on Se.idserie = P.idserie 
			inner join SAISON Sa on Sa.idserie = Se.idserie and Sa.idsaison = P.idsaison 
			where Se.titre = 'Navarro' and P.idacteur = A.idacteur) 
	and '10' != ALL(
		select Sa.idsaison 
			from PARTICIPATION P 
			inner join SERIE Se on Se.idserie = P.idserie 
			inner join SAISON Sa on Sa.idserie = Se.idserie and Sa.idsaison = P.idsaison 
			where Se.titre = 'Navarro' and P.idacteur = A.idacteur);

-- avec un join et un sous-select
select distinct A.nom 
	from ACTEUR A 
	inner join PARTICIPATION P on P.idacteur = A.idacteur
	inner join SERIE Se on Se.idserie = P.idserie 
	inner join SAISON Sa on Sa.idserie = Se.idserie and Sa.idsaison = P.idsaison 
	where Se.titre = 'Navarro' and Sa.idsaison="1"
	and not exists(
		select Sa.idsaison 
			from PARTICIPATION P 
			inner join SERIE Se on Se.idserie = P.idserie 
			inner join SAISON Sa on Sa.idserie = Se.idserie and Sa.idsaison = P.idsaison 
			where Se.titre = 'Navarro' and P.idacteur = A.idacteur and Sa.idsaison="10");

-- 4
\! echo "\n> Exercice 1.4 ----------------------------------------------------\n"
\! echo "Le titre des séries policières qui ont été diffusées à la fois sur ’TF1’ et ’D8’."
-- avec des sous-selects
select Se.titre 
	from SERIE Se 
	where Se.genre="policier"
	and exists (select * from DIFFUSION D where D.idserie = Se.idserie and D.nomchaine = 'TF1')
	and exists (select * from DIFFUSION D where D.idserie = Se.idserie and D.nomchaine = 'D8');
-- avec des joins
select Se.titre 
	from SERIE Se 
	inner join DIFFUSION D1 on D1.idserie = Se.idserie
	inner join DIFFUSION D2 on D2.idserie = Se.idserie
	where D1.nomchaine = 'TF1' and D2.nomchaine = 'D8' and Se.genre="policier";
-- avec un peu des deux
select Se.titre 
	from SERIE Se 
	inner join DIFFUSION D on D.idserie = Se.idserie
	where Se.genre="policier"
		and D.nomchaine = 'TF1' 
		and exists (select * from DIFFUSION D where D.idserie = Se.idserie and D.nomchaine = 'D8');

-- 5
\! echo "\n> Exercice 1.5 ----------------------------------------------------\n"
\! echo "Le nom de l’acteur qui a eu une somme de salaire (la somme des salaires de toutes les saisons) supérieure à celle de tous les autres acteurs dans la série ’Navarro’."
select A.* 
	from ACTEUR A 
	inner join PARTICIPATION P on A.idacteur = P.idacteur 
	inner join SERIE Se on P.idserie = Se.idserie 
	where Se.titre = 'Navarro' 
	group by A.idacteur 
	having sum(P.salaire) >= ALL(
		select sum(P.salaire) 
			from ACTEUR A 
			inner join PARTICIPATION P on A.idacteur = P.idacteur 
			inner join SERIE Se on P.idserie = Se.idserie 
			where Se.titre = 'Navarro' 
			group by A.idacteur);

-- 6
\! echo "\n> Exercice 1.6 ----------------------------------------------------\n"
\! echo "Créer une vue View Navarro qui contient le numéro de la saison Navarro, le nombre d’épisodes et le nombre d’acteurs qui y participent."
drop view if exists View_Navarro;
create view View_Navarro (saison,nbEpisodes,nbActeurs) as 
	select Sa.idsaison, count(distinct E.numepisode), count(distinct A.idacteur) 
		from SERIE Se 
		inner join SAISON Sa on Se.idserie = Sa.idserie 
		left outer join PARTICIPATION P on P.idserie = Se.idserie and P.idsaison = Sa.idsaison 
		left outer join ACTEUR A on A.idacteur = P.idacteur 
		left outer join EPISODE E on E.idserie = Se.idserie and E.idsaison = Sa.idsaison 
		where Se.titre = 'Navarro' 
		group by Sa.idsaison;

-- 7
\! echo "\n> Exercice 1.7 ----------------------------------------------------\n"
\! echo "Serait-il possible de supprimer une saison de la série Navarro de cette vue ? pourquoi ?"
-- non, plusieurs tables dans from
\! echo "Non, il y a plusieurs tables dans le from définissant la vue."




-- Exercice 2


-- 1
\! echo "\n> Exercice 2.1 ----------------------------------------------------\n"
\! echo "Nous appelons acteur principal d’une saison tout acteur ayant participé à tous les épisodes de cette saison. Écrire une procédure qui permet de vérifier si un acteur (passé en paramètre) est un acteur principal d’une saison (passée en paramètre) d’une série (passée aussi en paramètre). Dans l’affirmative, la procédure retourne 1, sinon elle retourne 0."
drop function if exists estSaisonPrincipal;
delimiter |
create function estSaisonPrincipal(idacteur int, idserie int, idsaison int) returns boolean deterministic
begin
	declare nbParticipations int;
	declare nbEpisodes int;
	select episodes into nbParticipations from PARTICIPATION P where P.idserie=idserie and P.idsaison=idsaison and P.idacteur=idacteur;
	select nbrepisodes into nbEpisodes from SAISON S where S.idserie=idserie and S.idsaison=idsaison;
	return ifnull(nbEpisodes = nbParticipations,0);
end|
delimiter ;

-- 2
\! echo "\n> Exercice 2.2 ----------------------------------------------------\n"
\! echo "Vérifier que ’Roger Hanin’ (idActeur = 101102) est un acteur principal de la première saison de la série ’Navarro’."
select estSaisonPrincipal(101102, 1, 1);

-- 3
\! echo "\n> Exercice 2.3 ----------------------------------------------------\n"
\! echo "Nous définissons un acteur comme étant principal d’une série s’il participe à toutes les saisons d’une série. Écrire une procédure qui permet de vérifier si un acteur (passé en paramètre) est un acteur principal d’une série (passée en paramètre). Dans l’affirmative, la procédure retourne 1, sinon elle retourne 0 (nous rappelons qu’une procédure peut faire appel à une autre)."
drop function if exists estSeriePrincipal;
delimiter |
create function estSeriePrincipal(idacteur int, idserie int) returns boolean deterministic
begin
	declare nbSaisonsPrincipals int;
	declare nbSaisons int;
	select count(*) into nbSaisonsPrincipals 
		from SAISON S 
		where S.idserie=idserie and estSaisonPrincipal(idacteur, idserie, S.idsaison);
	select count(*) into nbSaisons from SAISON S where S.idserie=idserie;
	return ifnull(nbSaisonsPrincipals = nbSaisons, 0);
end|
delimiter ;

-- 4
\! echo "\n> Exercice 2.4 ----------------------------------------------------\n"
\! echo "Afficher les noms de tous les acteurs principaux de la série ’Navarro’."
select A.* from ACTEUR A, SERIE S where estSeriePrincipal(A.idacteur, S.idserie) and S.titre="Navarro";

-- 5
\! echo "\n> Exercice 2.5 ----------------------------------------------------\n"
\! echo "Écrire un trigger qui permet de mettre à jour l’attribut nbrparticipation de la table Acteur. On incrémente le nbrparticipation d’un acteur s’il apparaı̂t en tant qu’acteur principal d’une saison de cette série."
drop trigger if exists nbParticipationInsertTrigger;
delimiter |
create trigger nbParticipationInsertTrigger after insert on PARTICIPATION
for each row
begin
	if estSaisonPrincipal(NEW.idacteur, NEW.idserie, NEW.idsaison) = 1 then
		update ACTEUR set nbParticipations = nbParticipations + 1 where idacteur=NEW.idacteur;
	end if;
end|
delimiter ;