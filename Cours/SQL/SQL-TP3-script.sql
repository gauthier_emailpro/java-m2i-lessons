CREATE DATABASE FILM;
USE FILM;

CREATE TABLE SERIE (
idserie int PRIMARY KEY,
titre varchar(35),
genre varchar(35),
realisateur varchar(35),
nbrsaisons int
)
;
insert into SERIE values (1,'Navarro','policier','gimiblat',0);
insert into SERIE values (2,'Lost','aventure','Jeffrey Lieber',0)
;
CREATE TABLE ACTEUR (
idacteur int PRIMARY KEY,
nom varchar(35),
nationalite varchar(35),
nbrparticipations int
);

insert into ACTEUR values (101102,'Roger Hanin','francaise',11);
insert into ACTEUR values (101103,'Emmanuelle Boidron','francaise',11);
insert into ACTEUR values (101104,'Filip Nikolic','francaise',3);
insert into ACTEUR values (101105,'Daniel Rialet','francaise',9);
insert into ACTEUR values (101106,'Matthew Fox','americaine',0);
insert into ACTEUR values (101107,'Josh Lee Holloway','americaine',0);
insert into ACTEUR values (101108,'Jorge Garcia','americaine',0);
insert into ACTEUR values (101109,'Evangeline Lilly','canadienne',0);

CREATE TABLE SAISON (
idserie int references SERIE (idserie),
idsaison int,
nbrepisodes int DEFAULT 0,
annee int,
PRIMARY KEY (idserie,idsaison) 
);

insert into SAISON values (1,1,3,1989);
insert into SAISON values (1,2,1,1990);
insert into SAISON values (1,3,0,1991);
insert into SAISON values (1,4,0,1992);
insert into SAISON values (1,5,0,1993);
insert into SAISON values (1,6,0,1994);
insert into SAISON values (1,7,0,1995);
insert into SAISON values (1,8,0,1997);
insert into SAISON values (1,9,0,1998);
insert into SAISON values (1,10,0,1999);
insert into SAISON values (1,11,0,2000);
insert into SAISON values (2,1,0,2004);
insert into SAISON values (2,2,0,2005);
insert into SAISON values (2,3,0,2006);
insert into SAISON values (2,4,0,2008);
insert into SAISON values (2,5,0,2009);
insert into SAISON values (2,6,0,2010);

CREATE TABLE PARTICIPATION (
idserie int references SERIE (idserie),
idsaison int references SAISON (idsaison),
idacteur int references ACTEUR (idacteur),
role varchar(35),
salaire int,
episodes int,
PRIMARY KEY (idserie,idsaison,idacteur) 
);

insert into PARTICIPATION values (1,1,101102,'Navarro',100000,3);
insert into PARTICIPATION values (1,2,101102,'Navarro',100000,1);
insert into PARTICIPATION values (1,3,101102,'Navarro',100000,0);
insert into PARTICIPATION values (1,4,101102,'Navarro',100000,0);
insert into PARTICIPATION values (1,5,101102,'Navarro',100000,0);
insert into PARTICIPATION values (1,6,101102,'Navarro',100000,0);
insert into PARTICIPATION values (1,7,101102,'Navarro',100000,0);
insert into PARTICIPATION values (1,8,101102,'Navarro',100000,0);
insert into PARTICIPATION values (1,9,101102,'Navarro',100000,0);
insert into PARTICIPATION values (1,10,101102,'Navarro',100000,0);
insert into PARTICIPATION values (1,11,101102,'Navarro',100000,0);
insert into PARTICIPATION values (1,1,101103,'YOLLANDE',100000,3);
insert into PARTICIPATION values (1,2,101103,'YOLLANDE',100000,1);
insert into PARTICIPATION values (1,3,101103,'YOLLANDE',100000,0);
insert into PARTICIPATION values (1,4,101103,'YOLLANDE',100000,0);
insert into PARTICIPATION values (1,5,101103,'YOLLANDE',100000,0);
insert into PARTICIPATION values (1,6,101103,'YOLLANDE',100000,0);
insert into PARTICIPATION values (1,7,101103,'YOLLANDE',100000,0);
insert into PARTICIPATION values (1,8,101103,'YOLLANDE',100000,0);
insert into PARTICIPATION values (1,9,101103,'YOLLANDE',100000,0);
insert into PARTICIPATION values (1,10,101103,'YOLLANDE',100000,0);
insert into PARTICIPATION values (1,11,101103,'YOLLANDE',100000,0);
insert into PARTICIPATION values (1,9,101104,'BOLDEC',100000,0);
insert into PARTICIPATION values (1,10,101104,'BOLDEC',100000,0);
insert into PARTICIPATION values (1,11,101104,'BOLDEC',100000,0);
insert into PARTICIPATION values (1,1,101105,'BLOMET',100000,3);
insert into PARTICIPATION values (1,2,101105,'BLOMET',100000,1);
insert into PARTICIPATION values (1,3,101105,'BLOMET',100000,0);
insert into PARTICIPATION values (1,4,101105,'BLOMET',100000,0);
insert into PARTICIPATION values (1,5,101105,'BLOMET',100000,0);
insert into PARTICIPATION values (1,6,101105,'BLOMET',50000,0);
insert into PARTICIPATION values (1,7,101105,'BLOMET',100000,0);
insert into PARTICIPATION values (1,8,101105,'BLOMET',90000,0);
insert into PARTICIPATION values (1,9,101105,'BLOMET',90000,0);

CREATE TABLE EPISODE (
idserie int references SERIE (idserie),
idsaison int references SAISON (idsaison),
numepisode int,
titreepisode varchar(35),
duree int,
PRIMARY KEY (idserie,idsaison,numepisode) 
);

insert into EPISODE values (1,1,1,'Folies de flic',80);
insert into EPISODE values (1,1,2,'Un rouleau',90);
insert into EPISODE values (1,1,3,'La fille d\'André',85);
insert into EPISODE values (1,2,1,'Fils de périph',80);


CREATE TABLE DIFFUSION (
idserie int references SERIE (idserie),
idsaison int references SAISON (idsaison),
numepisode int references EPISODE (numepisode),
nomchaine varchar(35),
dated timestamp,
PRIMARY KEY (idserie,idsaison,numepisode,nomchaine,dated) 
)
;
insert into DIFFUSION values (1,1,1,'TF1',now());
insert into DIFFUSION values (1,1,2,'TF1',now());
insert into DIFFUSION values (1,1,3,'TF1',now());
insert into DIFFUSION values (1,2,1,'TF1',now());
insert into DIFFUSION values (1,1,1,'D8',now());

