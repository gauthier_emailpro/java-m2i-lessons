
// 4.A
document.body.insertAdjacentHTML("afterbegin", "Apres le begin");
document.body.insertAdjacentHTML("beforeend", "Avant le end");

// 4.B
var h12 = document.querySelectorAll("h1,h2");
for (var e of h12) {
	console.log(e);
	e.innerHTML = "Titre" + e.innerHTML;
}

// 4.C
document.getElementById("souligne").addEventListener("click", function(){
	var ps = document.getElementsByTagName("p");
	for (p of ps)
		p.style.textDecoration = "underline";
})
document.getElementById("gras").addEventListener("click", function(){
	var ps = document.getElementsByTagName("p");
	for (p of ps)
		p.style.fontWeight = "bold";
})
document.getElementById("italique").addEventListener("click", function(){
	var ps = document.getElementsByTagName("p");
	for (p of ps)
		p.style.fontStyle = "italic";
})
document.getElementById("reset").addEventListener("click", function(){
	var ps = document.getElementsByTagName("p");
	for (p of ps) {
		p.style.textDecoration = "none";
		p.style.fontWeight = "normal";
		p.style.fontStyle = "normal";
	}
})