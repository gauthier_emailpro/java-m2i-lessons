
// 5.B
function checkCompleted() {
	var inputs = document.getElementsByTagName("input");
	var nbChecked = 0;
	for (i of inputs) {
		if (i.checked)
			nbChecked++;
	}
	return (nbChecked == 2);
}

var inputs = document.getElementsByTagName("input");
for (i of inputs) {
	i.addEventListener("click", function() {
		if (checkCompleted()) {
			document.getElementById("submit").disabled = false;
		}
	})
}


// 5.C

// objet contenant les couples (question, reponse)
var response = {q1:"a", q2:"b"};

// fonction validant les champs
function checkAnswers() {
	console.log("validation");
	var ret = true;
	var inputs = document.getElementsByTagName("input");
	for (i of inputs) {
		if (i.checked) {
			if (i.value == response[i.name]) {
				console.log("reponse " + i.name + " validée");
				document.getElementById(i.name+"l").style.borderColor = "green";
			} else {
				console.log("reponse " + i.name + " fausse");
				document.getElementById(i.name+"l").style.borderColor = "red";
				ret = false;
			}

		}
	}
	return ret;
}

// document.getElementById("validate").addEventListener("submit", function() { console.log("1"); checkAnswers(); });
