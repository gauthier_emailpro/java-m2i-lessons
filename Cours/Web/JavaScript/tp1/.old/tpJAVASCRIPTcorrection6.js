
// 6.A ////////////////////////////////////////////////////////////////////////////////////////////
document.getElementById("newWindow").addEventListener("click", function() {
	window.open("https://www.w3schools.com", "_blank", "height="+ window.screen.height/2 +",width="+ window.screen.width/2, "false");
});


// 6.B,C //////////////////////////////////////////////////////////////////////////////////////////

// fonction qui recupere le nom par un prompt et l'enregistre dans un cookie
function promptCookie() {
	var nom;
	do {
		nom = prompt("Nom ?");
	} while (nom == "" || nom == null);
	setCookie("nom", nom, 15);
}

// Si le cookie n'existe pas : prompt
if (getCookie("nom") == "") {
	promptCookie();
}

// Affichage du message d'acceuil
document.getElementById("greating").innerHTML = "Welcome " + getCookie("nom");

// Evenement du bouton pour le choix du nom
document.getElementById("selectName").addEventListener("click", function() {
	promptCookie();
	document.getElementById("greating").innerHTML = "Welcome " + getCookie("nom");
});

// bouton de suppression du cookie
document.getElementById("deleteCookie").addEventListener("click", function() {
	deleteCookie("nom");
});


// 6.D ////////////////////////////////////////////////////////////////////////////////////////////
document.getElementById("greating").addEventListener("mouseover", function(){
	this.style.color = "red";
});
document.getElementById("greating").addEventListener("mouseout", function(){
	this.style.color = "black";
});


// W3schools function to manipulate coockies //////////////////////////////////////////////////////

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function deleteCookie(cname) {
    document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
}