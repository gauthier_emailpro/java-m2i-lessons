
// 5.B

function updateSubmitButton() {
	// recuperation de la liste des input radio
	var irs = document.querySelectorAll("input[type='radio']");
	// compte les input checked
	var nb = 0;
	for (let i of irs) {
		if (i.checked)
			nb++;
	}
	// si tous les questions sont checked
	if (nb == 2)
		// on active le submit
		document.querySelector("#submit").disabled = false;
}

// ajout des event listener sur les inputs radio
// avec lambda
document.querySelectorAll("input[type='radio']")
	.forEach(i => i.addEventListener("click", updateSubmitButton));
// // sans lambda
// var irs = document.querySelectorAll("input[type='radio']");
// for (var ir of irs)
// 	ir.addEventListener("click", updateSubmitButton);


// 5.C

// objet contenant les couples (question, reponse)
var response = { q1: "a", q2: "b" };

function checkAnswers(evt) {
	var submit = true;
	document.querySelectorAll("input[type='radio']:checked").forEach(i => {
		i.parentNode.style.borderColor = (i.value == response[i.name]) ? "green" : "red";
		submit = submit && (i.value == response[i.name]);
	});
	if (!submit) {
		evt.preventDefault();
	}
}

document.querySelector("form").addEventListener("submit", checkAnswers);
