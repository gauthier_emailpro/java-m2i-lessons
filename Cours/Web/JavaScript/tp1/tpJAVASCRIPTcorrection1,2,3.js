
// 1
document.write("Exercice 1<br>");
document.write(new Date() + "<br>");


// 2.A
document.write("<br>Exercice 2.A<br>");
var x = 123;
var y = x + 10;
document.write(y + "<br>");


// 2.B
document.write("Exercice 2.B<br>");
var x = "123";
var y = x + 10;
document.write(y + "<br>");


// 2.C
document.write("Exercice 2.C<br>");
var x = "je suis globale";
var y = "je suis globale";
function test() {
	var x = "je suis locale"; // on définit une variable x locale qui "masque" la variable x globale
	var z = "je suis locale"; // on définit une variable z locale
	document.write("dans la fonction :<br>");
	document.write("x (" + typeof x + ") = " + x + "<br>"); // x est la variable locale (x globale est masquée)
	document.write("y (" + typeof y + ") = " + y + "<br>"); // y est accessible (les variables globales non masquées sont accessibles)
	document.write("z (" + typeof z + ") = " + z + "<br>"); // z est accessible
}
test();
document.write("dans le script :<br>");
document.write("x (" + typeof x + ") = " + x + "<br>");     // x est la variable globale (les variables locales à la fonction ne sont pas accessibles en dehors de celle-ci)
document.write("y (" + typeof y + ") = " + y + "<br>");     // y est accessible
//document.write("z (" + typeof z + ") = " + z + "<br>");   // z n'est pas accessible et n'existe pas ici (les variables locales à la fonction ne sont pas accessibles en dehors de celle-ci)


// 3.A
document.write("<br>Exercice 3.A<br>");
// on créer un tableau
var t = [];
// on ajoute des elements au tableau
for (var i=0; i < 10; i+=2) {
	t.push(i);
}
// affichage du tableau
document.write(t + "<br>");


// 3.B
document.write("<br>Exercice 3.B<br>");
// calcul de la somme avec un for .. of ..
var sum = 0;
for (var e of t) {
	sum += e;
}
document.write(sum + "<br>");
// calcul de la somme avec un for classique
sum = 0;
for (var i=0; i<5; ++i) {
	sum += t[i];
}
document.write(sum + "<br>");
// calcul de la somme avec reduce()
function computeSum(total, value, index, array) {
	return total + value;
}
sum = t.reduce(computeSum);
document.write(sum + "<br>");
// calcul de la somme avec reduce() et lambda !!
sum = t.reduce((t, v, i, a) => t+v);
document.write(sum + "<br>");


// 3.C
// remplissage du tableau
document.write("<br>Exercice 3.C<br>");
// on créer un tableau
var prod = [];
for (var i=0; i<10; ++i) {
	// chaque element du tableau prod est lui-même un tableau
	prod[i] = [];
	// on remplit ce deuxime tableau
	for (var j=0; j<10; ++j) {
		prod[i][j] = (i+1)*(j+1);
	}
}
document.write(prod + "<br>");


// 3.D
// affichage du tableau
document.write("<br>Exercice 3.D<br>");
// creation de la table et affichage de la première ligne
document.write("<table  style=\"width:50%\"><tr><th>x</th>");
for (var i=1; i<=10; ++i) {
	document.write("<th>" + i + "</th>");
}
document.write("</tr>");
// parcours du tableau ligne à ligne
for (var i=0; i<10; ++i) {
	// pour chaque ligne, on ajoute un en-tête (de ligne)
	document.write("<tr><th>" + (i+1) + "</th>");
	// puis on affiche les valeurs du tableau
	for (var j=0; j<10; ++j) {
		document.write("<td>" + prod[i][j] + "</td>");
	}
	document.write("</tr>");
}
document.write("</table>");
