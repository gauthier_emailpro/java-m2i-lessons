// 4.A
var body = document.querySelector("body");
// ajout à la fin
body.insertAdjacentHTML("beforeend", "<p>à la fin avec <code>insertAdjacentHTML</code></p>");
// ajouter au debut
body.insertAdjacentHTML("afterbegin", "<p>au début avec <code>insertAdjacentHTML</code></p>");

// 4.B
// recuperation de tous les elements de type <h1> ou <h2>
var titles = document.querySelectorAll("h1, h2");
// pour ces elements, ajout de "Titre " au debut
for (var t of titles) {
	t.innerHTML = "Titre " + t.innerHTML;
}

// 4.C
var actions = {
	bold: (a) => a.style.fontWeight = (a.style.fontWeight == "bold") ? "normal" : "bold",
	italic: (a) => a.style.fontStyle = (a.style.fontStyle == "italic") ? "normal" : "italic",
	underline: (a) => a.style.textDecoration = (a.style.textDecoration == "underline") ? "none" : "underline",
	reset: (a) => {
		a.style.fontWeight = "normal";
		a.style.textDecoration = "none";
		a.style.fontStyle = "normal";
	}
};

function restyleFunction2(e) {
	document.querySelectorAll(".auteur").forEach(a => actions[e.target.dataset.action](a));
}

document.querySelectorAll(".style-button").forEach(b =>
	b.addEventListener("click", restyleFunction2));





// // ajout d'un gestionnaire d'evenement pour "click" sur l'element d'id "souligne"
// document.getElementById("souligne").addEventListener("click", function () {
// 	// recuperation des elements ayant la classe "auteur"
// 	var auteurs = document.getElementsByClassName("auteur");
// 	// pour chacun des elements, ajout de la propriete CSS textDecoration = underline
// 	for (var a of auteurs) {
// 		if (a.style.textDecoration == "underline")
// 			a.style.textDecoration = "none";
// 		else
// 			a.style.textDecoration = "underline";
// 	}
// });
// // idem precedent, mais met en gras
// document.getElementById("gras").addEventListener("click", function () {
// 	var auteurs = document.getElementsByClassName("auteur");
// 	for (var a of auteurs) {
// 		a.style.fontWeight = "bold";
// 	}
// });
// // idem precedent, mais met en italique
// document.getElementById("italique").addEventListener("click", function () {
// 	var auteurs = document.getElementsByClassName("auteur");
// 	for (var a of auteurs) {
// 		a.style.fontStyle = "italic";
// 	}
// });
// // idem precedent, mais remet à zéro les styles
// document.getElementById("reset").addEventListener("click", function () {
// 	var auteurs = document.getElementsByClassName("auteur");
// 	for (var a of auteurs) {
// 		a.style.fontStyle = "normal";
// 		a.style.fontWeight = "normal";
// 		a.style.textDecoration = "none";
// 	}
// });