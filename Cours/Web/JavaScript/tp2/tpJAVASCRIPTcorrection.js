



var operations = {};
operations.add = function(x, y) { return x+y; };
operations.sub = function(x, y) { return x-y; };
operations.mult = function(x, y) { return x*y; };
operations.div = function(x, y) { return x/y; };
operations.mod = function(x, y) { return x%y; };
operations.abs = function(x) { return Math.abs(x); };
operations.fac = function(x) { return factorial(x); }

var operationsType = {};
operationsType.add = "binary";
operationsType.sub = "binary";
operationsType.mult = "binary";
operationsType.div = "binary";
operationsType.mod = "binary";
operationsType.abs = "unary";
operationsType.fac = "unary";



document.getElementById("eq").addEventListener("click", function() {
	var x = parseInt(document.getElementById("x").value);
	var y = parseInt(document.getElementById("y").value);
	var op = document.getElementById("op").value;
	// console.log(x + " " + op + " " + y);
	document.getElementById("result").innerHTML = operations[op](x,y);
	// ou
	// var result;
	// switch (op) {
	// 	case "add" :
	// 		result = x+y;
	// 		break;
	// 	case "sub" :
	// 		result = x-y;
	// 		break;
	// 	case "mult" :
	// 		result = x*y;
	// 		break;
	// 	case "div" :
	// 		result = x/y;
	// 		break;
	// 	case "mod" :
	// 		result = x%y;
	// 		break;
	// 	case "abs" :
	// 		result = Math.abs(x);
	// 		break;
	// 	case "fac" :
	// 		result = factorial(x);
	// 		break;
	// }
	// document.getElementById("result").innerHTML = result;
});

document.getElementById("op").addEventListener("change", function() {
	if (operationsType[this.value] == "unary")
		document.getElementById("y").disabled = true;
	else
		document.getElementById("y").disabled = false;
	// ou
	// switch (this.value) {
	// 	case "add" :
	// 	case "sub" :
	// 	case "mult" :
	// 	case "div" :
	// 	case "mod" :
	// 	document.getElementById("y").disabled = false;
	// 	break;
	// 	case "abs" :
	// 	case "fac" :
	// 	document.getElementById("y").disabled = true;
	// 	break;
	// }
});


function factorial(x) {
	if (x > 1)
		return x*factorial(x-1);
	else
		return 1;
}












// // 6.A ////////////////////////////////////////////////////////////////////////////////////////////
// document.getElementById("newWindow").addEventListener("click", function() {
// 	window.open("https://www.w3schools.com", "_blank", "height="+ window.screen.height/2 +",width="+ window.screen.width/2, "false");
// });


// // 6.B,C //////////////////////////////////////////////////////////////////////////////////////////

// // fonction qui recupere le nom par un prompt et l'enregistre dans un cookie
// function promptCookie() {
// 	var nom;
// 	do {
// 		nom = prompt("Nom ?");
// 	} while (nom == "" || nom == null);
// 	setCookie("nom", nom, 15);
// }

// // Si le cookie n'existe pas : prompt
// if (getCookie("nom") == "") {
// 	promptCookie();
// }

// // Affichage du message d'acceuil
// document.getElementById("greating").innerHTML = "Welcome " + getCookie("nom");

// // Evenement du bouton pour le choix du nom
// document.getElementById("selectName").addEventListener("click", function() {
// 	promptCookie();
// 	document.getElementById("greating").innerHTML = "Welcome " + getCookie("nom");
// });

// // bouton de suppression du cookie
// document.getElementById("deleteCookie").addEventListener("click", function() {
// 	deleteCookie("nom");
// });


// // 6.D ////////////////////////////////////////////////////////////////////////////////////////////
// document.getElementById("greating").addEventListener("mouseover", function(){
// 	this.style.color = "red";
// });
// document.getElementById("greating").addEventListener("mouseout", function(){
// 	this.style.color = "black";
// });


// // W3schools function to manipulate coockies //////////////////////////////////////////////////////

// function setCookie(cname, cvalue, exdays) {
//     var d = new Date();
//     d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
//     var expires = "expires="+d.toUTCString();
//     document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
// }

// function getCookie(cname) {
//     var name = cname + "=";
//     var ca = document.cookie.split(';');
//     for(var i = 0; i < ca.length; i++) {
//         var c = ca[i];
//         while (c.charAt(0) == ' ') {
//             c = c.substring(1);
//         }
//         if (c.indexOf(name) == 0) {
//             return c.substring(name.length, c.length);
//         }
//     }
//     return "";
// }

// function deleteCookie(cname) {
//     document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
// }