var container = window.document.getElementById("container");
console.log(container.innerText);
container.innerHTML = "<p>Salut !</p>" + container.innerHTML;
console.log(container.parentNode);
console.log(container.parentNode.nodeName);

// ajout par objet 
var par = document.createElement("p");
par.id = "intro";
par.setAttribute("class", "blue");
var text = document.createTextNode("JS paragraph (object)");
par.appendChild(text);
var container = document.getElementById("container");
container.appendChild(par);

// ajout par string
container.innerHTML += "<p id='intro1' class='red'>JS paragraph (string)</p>";