window.onload = function () {

	// exercice 1.3
	function controler() {
		let zoneSaisie = document.getElementById("input13");
		alert("La Zone de saisie contient : " + zoneSaisie.value);
	}
	document.getElementById("button13").addEventListener("click", controler);

	// exercice 1.4
	function controlerWithError() {
		let zoneSaisie = document.getElementById("input14");
		alert("La Zone de saisie " +((zoneSaisie.value) ? " contient : " + zoneSaisie.value : "ne contient rien"));
	}
	document.getElementById("button14").addEventListener("click", controlerWithError);

	// exercice 1.5
	function evaluer() {
		var expr = document.getElementById("expression").value;
		document.getElementById("result").innerHTML = "Résultat : " + eval(expr);
	}
	document.getElementById("submit").addEventListener("click", evaluer);

	// exercice 2
	function valider() {
		var input = document.getElementById("input");
		var output = document.getElementById("output");
		if (input.value == "" || isNaN(input.value)) {
			alert("Valeur invalide.");
		} else {
			output.value = Math.abs(input.value);
		}
	}
	document.getElementById("output").readOnly = true;
	document.getElementById("submit2").addEventListener("click", valider);



}