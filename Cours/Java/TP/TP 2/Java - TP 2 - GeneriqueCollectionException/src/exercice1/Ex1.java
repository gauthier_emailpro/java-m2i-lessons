package exercice1;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Ex1 {
	
	public static void main(String [] ars) {
		
		// version correspondant a l'enonce ///////////////////////////////////////////////////////
		// definition de la map
		Map<Integer, String> m = new HashMap<>();
		m.put(7, "paris");
		m.put(5, "rome");
		m.put(1, "manchester");
		m.put(3, "barcelone");
		// chaine de caractere a analyser
		String str = "Manchester ou Barcelone, mais jamais paris ou rome.";
		// on decoupe la chaine autour des separateurs ' ' et ','
		String[] tokens = str.split("[ ,]");
		// on creer une chaine vide pour stocker le resultat
		String result = "";
		// on parcours la liste des tokens (resultat du split)
		for (String token : tokens) {
			// un booleen pour memoriser si le token a ete remplacer
			boolean replaced = false;
			// on parcors la liste des couple (cle,val) de la map
			for (Entry<Integer, String> e : m.entrySet()) {
				// si val correspond au token (avec conversion en miniscule pour eviter les conflits de casse)
				if (token.toLowerCase().equals(e.getValue().toLowerCase())) {
					// on inject la cle dans la chaine de resultat
					result += e.getKey() + " ";
					// on met a jour le booleen
					replaced = true;
					break;
				}
			}
			// si le token n'a pas ete trouve
			if (!replaced)
				// on injecte la valeur oriinale dans le resultat
				result += token + " ";
		}
		System.out.println(result);

		// version custom, en inversant les cle et les valeurs ////////////////////////////////////
		// definition de la map
		Map<String, Integer> m1 = new HashMap<>();
		m1.put("paris", 7);
		m1.put("rome", 5);
		m1.put("manchester", 1);
		m1.put("barcelone", 3);
		// chaine de caractere a analyser
		String str1 = "Manchester ou Barcelone, mais jamais paris ou rome.";
		// on decoupe la chaine autour des separateurs ' ' et ','
		String[] tokens1 = str1.split("[ ,]");
		// on creer une chaine vide pour stocker le resultat
		String result1 = "";
		// on parcours la liste des tokens (resultat du split)
		for (String token : tokens1) {
			// on recupere la valeur associee a la cle token
			Integer v = m1.get(token.toLowerCase());
			if (v == null)
				// si pas de valeur => on injecte la valeur oriinale dans le resultat
				result1 += token + " ";
			else
				// sinon => on injecte la cle dans la chaine de resultat
				result1 += v + " ";
		}
		System.out.println(result1);
	}

}
