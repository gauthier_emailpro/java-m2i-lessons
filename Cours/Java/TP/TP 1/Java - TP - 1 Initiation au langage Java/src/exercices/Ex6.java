package exercices;

import java.util.Scanner;

public class Ex6 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		// saisir une valeur
		System.out.println("Sasir un nombre :");
		int x = s.nextInt();
		
		// calculde la somme des x premiers entiers
		int sum = 0;
		for (int i=1; i<=x; ++i)
			sum += i;
		System.out.println("somme des " + x + " premiers entiers : " + sum);
		
		s.close();
	}

}
