package exercices;

import java.util.Scanner;

public class Ex9 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		int x, max = Integer.MIN_VALUE, imax = -1, i;
		
		i = 0;
		do {
			System.out.print("Saisir nombre n°" + i + " : ");
			x = s.nextInt();
			if ((x != 0) && (x > max)) {
				max = x;
				imax = i;
			}
			i++;
		} while (x != 0);
		System.out.println("Le plus grand est : " + max + " (position " + imax + ")");
		
	
		
		s.close();
	}

}
