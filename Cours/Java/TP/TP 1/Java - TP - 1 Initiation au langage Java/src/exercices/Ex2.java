package exercices;

import java.util.Scanner;

public class Ex2 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);

		int x;
		do {
			System.out.println("x (in [10,20]) ?");
			x = s.nextInt();
			if (x < 10)
				System.out.println("Error, try a larger value.");
			else if (x > 20)
				System.out.println("Error, try a smaller value");
		} while (x < 10 || x > 20);
		System.out.println("Congrats ! You have entered " + x);

		s.close();
	}

}
