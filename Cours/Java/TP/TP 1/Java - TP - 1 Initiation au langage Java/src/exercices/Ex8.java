package exercices;

import java.util.Scanner;

public class Ex8 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		int max = Integer.MIN_VALUE, imax = -1;
		
		for (int i = 1; i<=20; ++i) {
			System.out.print("Saisir nombre n°" + i + " : ");
			int x = s.nextInt();
			if (x > max) {
				max = x;
				imax = i;
			}
		}
		System.out.println("Le plus grand est : " + max + " (position " + imax + ")");
		
		s.close();
	}

}
