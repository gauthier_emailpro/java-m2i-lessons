package exercices;

import java.util.Scanner;

public class Ex3 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		// saisir une valeur
		System.out.println("Sasir un nombre :");
		int x = s.nextInt();
		
		// afficher les dix suivantes avec un for
		System.out.println("Les dix suivants :");
		for (int i=1; i<=10; ++i)
			System.out.print(x+i+" ");
		System.out.println();

		// afficher les dix suivantes avec un while
		System.out.println("Les dix suivants :");
		int i=1;
		while (i<=10) {
			System.out.print(x+i+" ");
			i++;
		}
		System.out.println();
		
		s.close();
	}

}
