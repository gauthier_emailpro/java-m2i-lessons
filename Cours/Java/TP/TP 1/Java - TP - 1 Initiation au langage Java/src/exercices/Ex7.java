package exercices;

import java.util.Scanner;

public class Ex7 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		// saisir une valeur
		System.out.println("Sasir un nombre :");
		int x = s.nextInt();
		
		// calculde la somme des x premiers entiers
		int fact = 1;
		for (int i=1; i<=x; ++i)
			fact *= i;
		System.out.println("factorielle de " + x + " : " + fact);
		
		s.close();
	}

}
