package exercices;

import java.util.Scanner;

public class Ex5 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		// saisir une valeur
		System.out.println("Sasir un nombre :");
		int x = s.nextInt();
		
		// affichage de la table de multiplication
		System.out.println("Table de " + x);
		for (int i=1; i<=10; ++i)
			System.out.println(x + " x " + i + " = " + x*i);
		
		s.close();
	}

}
