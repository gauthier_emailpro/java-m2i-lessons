package exercices;

import java.util.Scanner;

public class Ex11 {

	
	public static int fact(int x) {
		int fact = 1;
		for (int i=1; i<=x; ++i)
			fact *= i;
		return fact;
	}
	
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		int n, p;
		
		System.out.println("Nombre de chevaux : ");
		n = s.nextInt();
		System.out.println("Nombre de propositions : ");
		p = s.nextInt();
		
		double x = fact(n)/fact(n-p);
		double y = fact(n)/(fact(p)*fact(n-p));
		
		System.out.println("Dans l’ordre : une chance sur " + x + " de gagner");
		System.out.println("Dans le désordre : une chance sur " + y + " de gagner");
		
		s.close();
	}

}
