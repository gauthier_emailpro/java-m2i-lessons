package exercices;

import java.util.Scanner;

public class Ex10 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		int doit = 0, paye = 0, prix;
		
		System.out.println("Saisir les prix :");
		do {
			prix = s.nextInt();
			if (prix != 0)
				doit += prix;
		} while (prix != 0);
		System.out.println("Le montant du est : " + doit);
		
		System.out.println("Montant paye : ");
		paye = s.nextInt();
		
		int aRendre = paye-doit;
		int aRendre10 = aRendre/10;
		int aRendre5 = (aRendre%10)/5;
		int aRendre1 = aRendre%5;
		
		System.out.println("Le montant à rendre est : " + aRendre);
		System.out.println("\t10e : " + aRendre10);
		System.out.println("\t5e : " + aRendre5);
		System.out.println("\t1e : " + aRendre1);
	
		
		s.close();
	}

}
