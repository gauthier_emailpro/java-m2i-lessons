package exercices;

import java.util.Scanner;

public class Ex1 {

	public static void main(String[] args) {
		int x;
		Scanner s = new Scanner(System.in);

		do {
			System.out.println("x (in [1,3]) ?");
			x = s.nextInt();
			if (x < 1 || x > 3)
				System.out.println("Wrong ! Try again.");
		} while (x < 1 || x > 3);
		System.out.println("Congrats ! You have entered " + x);

		s.close();
	}

}
