# Authentification

On veut permettre l'authentification d'utilisateur par notre application web angular. En particulier, un utilsateur doit pouvoir :
- créer un nouveau compte (signup) s'il n'est pas déjà connecté
- se connecter (signin) s'il n'est pas déjà connecté
- se déconnecter s'il est déjà connecté

## Service et composant angular

On se contentera dans un premier temps de mettre en place dans notre application les éléments nécessaires à l'authentification.

Nous placerons tous les éléments relatifs à l'authentification dans une sous module `AuthenticationModule`. Celui-ci contiendra :
- `AuthenticationService` : service qui memorisera le membre connecté et fournira des méthodes pour se connecter, créer un compte, se déconnecter et accéder aux informations sur le membre connecté.
- `SigninComponent` : composant permettant d'afficher la page de connexion (ou un modal ?). Utilisera le service pour vérifier les *credentials* entrés par l'utilisateur. 
- `SignupComponent` : composant permettant de créer un compte. Peut utiliser le composant `MembersFormComponent` déjà existant.


#### Module authentication
- créez le nouveau module

#### Service Authentication
- dans le module précedemment créé, ajoutez un nouveau service `authentication`
- injectez `HttpClient` pour faire des requètes vers le web-service
- ajoutez un attribut de type `Member` pour stocker les informations sur l'utilisateur connecté
- ajoutez les méthodes :
    - `signin` qui prend en paramètre un email, un password, une méthode à appeler en cas de succès et une méthode à appeler en cas d'erreur. Elle devra faire une requète POST vers l'url `http://localhost:8080/graze-webservice/authentication/signin`, puis :
        - en cas de succès : enregistre le membre renvoyé par le serveur dans l'attribut et appel le callback passé en paramètre
        - en cas d'erreur : appel le callback passé en paramètre
    - `signup` qui prend en paramètre un objet `Member` et fait une requète POST vers l'url `http://localhost:8080/graze-webservice/authentication/signup`. Elle devra renvoyé un observable.
    - `signout` qui donne la valeur undefined à l'attribut member

#### Composant Signin

#### Composant Signup





