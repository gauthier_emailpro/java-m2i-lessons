## Ajout et édition de membres

On veut factoriser le formulaire d'ajout et de création de membre en créant un composant `members-form`. Celui prendra en paramètres (`@Input`) le titre à afficher, le label du bouton de soumission et, éventuellement, le membre à éditer. Lors de la soumission, il enverra un signal (`@Output`) contenant le nouvel objet `Member` à son parent (`members-add` ou `members-edit`).

#### Mise en place du formulaire générique
- créer le composant `members-form`
- dans `members-form.component.ts` :
    - ajoutez les attributs `title`, `label` et `member`
- dans `members-form.component.html` :
    - copiez/collez le formulaire d'ajout de membre depuis `members-add-component.html`
    - modifiez-le pour inclure (par interpolation) le titre et le label du bouton de soumission, et par *two ways binding* liez l'attribut `member` aux inputs
- dans `members-add-component.html` et `members-edit-component.html` :
    - incluez la balise du composant `members-form`

#### Passage de paramètres au formulaire générique
- dans `members-form.component.ts` :
    - ajoutez le décorateur `@Input` sur les attributs `title`, `label` et `member`
- dans `members-add-component.html` et `members-edit-component.html` :
    - passer les valeurs de `title` et `label` à `members-form`
- testez

#### Écoute de l'évennement `submit` par `members-add` et `members-edit`
- dans `members-form.component.ts` :
    - ajouter un attribut `submit` de type `EventEmitter<Member>` avec le décorateur `@Output`
    - lors de la soumission, appellez la méthode `emit` de l'attribut en lui passant en paramètre l'objet membre
- dans `members-add-component.html` et `members-edit-component.html` :
    - sur la balise du composant `members-form`, ajouter un *event binding* pour l'évennement `submit`. Celui-ci doit appeler une méthode qui : (1) appelle la méthode de `MemberService` adéquate et (2) redirige le navigateur vers la page `members `.

#### Rendre fonctionnelle l'édition de membre
On veut ici associer le bouton d'édition du composant `members` à la page d'édition de membre. 
- dans `members.component.ts`, modifiez le boutton d'édition de membre pour que celui-ci soit un lien et dirige (`routerLink`, pas `href` !) vers l'url `members/edit/123` où 123 est l'id du membre a éditer.
- modiiez le routage du module membre pour inclure le paramètre d'url `:id`
- dans `members-edit-component.ts` :
    - récupérez par injection `MemberService` (si ce n'est pas déjà fait) et `ActivatedRoute`.
    - ajoutez un attribut `member`
    - dans `ngOnInit()`, lire le paramètre de chemin `id` et récupérer l'objet membre correspondant.
- dans `members-edit-component.html` :
    - passez en paramètre de `members-form` l'attribut `member`.


## Affichage des membres en liste ou grille

On veut permettre l'affichage des membres sous deux modes : liste ou grille. Pour chaque mode, on créera un composant permettant d'afficher un membre. Un (ou plusieurs) bouton(s) permettront de passer d'un mode à l'autre. A terme, on voudra enregistrer dans une cookie le mode d'affichage choisi par l'utilisateur.

#### Composants permettant d'afficher un membre
- créez les composants `members-row` et `members-card` dans le module `members`
- pour chaque composant :
    - dans le ts, ajouter un attribut de type `Member` avec un `@Input`
    - complétez les html/css pour qu'il affiche l'attribut membre (faites un truc pas trop dégeu)
    - le bouton edit doit dirigé vers l'url `members/edit/123` (où 123 est l'id de l'attribut membre)
    - le bouton rm envoi un évennement `delete` au composant parent (vous aurez besoin d'un attribut de type `EventEmitter` avec le décorateur `@Output`)

#### Composant affichant la liste des membres
- dans `members-component.ts` : ajouter un attribut `displayType` de type `string`, qui prendra les valeurs "grid" ou "rows"
- dans `members-component.html` : 
    - avec un `*ngIf` (en plus du `*ngFor`), utiliser les composants `members-row` ou `members-card` suivant la valeur de l'attribut `displayType`. Vous devrez leur passer en paramètre (propriété de la balise) le membre à afficher et associer à l'évennement `delete` une fonction `onDelete(id: number)`)
    - ajouter un (ou plusieurs) bouton(s) permettant de changer la valeur de l'attribut `displayType` (par exemple des inputs de type radio liés à `displayType` par *two ways binding*)
- dans `members-component.ts` : créez la méthode `onDelete(id: number)` qui supprime le membre dont l'id est passé en paramètre.


#### Si vous êtes chaud (ou désœuvré)
Mémorisez le mode d'affichage choisi par l'utilisateur dans un cookie. Regarder la documentation d'angular pour la gestion des cookies : [ici](http://www.google.com) !

L'idée est d'enregistrer dans un cookie la valeur de l'attribut `displayType`. Au chargement du composant (`ngOnInit()`), lire la valeur du cookie. Lorsque l'attribut change (evenement `ngModelChange`), l'enregistrer (le cookie).


## Champs de recherche/filtrage pour les membres

On veut ajouter un champ de recherche à la page des membres. Celui-ci premettra de n'afficher que les membres dont l'alias ou l'email contient la chaine de caractères recherchée. Cette chaine devra apparaitre dans les paramètres de la requète. 

#### Préparation du service
- dans `members-service.ts`, ajouter une méthode `findByAliasOrEmailLike(str: string)` qui prend en paramètre une`string` et renvoie la liste des membres dont l'alias ou l'email contient la string.

#### Composant affichant la liste des membres
- dans `members-component.ts` : 
    - ajouter un attribut `searchPattern` de type `string`
    - dans `ngOnInit` récupérer le paramètre de requète et mémoriser sa valeur dans `searchPattern` (vous aurez besoin d'injecter `ActivatedRoute`, puis dans `ngOnInit()` de faire un truc du genre `this.ar.queryParamMap.subscribe(...)`)
    - ajouter une méthode `onSearchPatternChange` qui appelle `findByAliasOrEmailLike` de `MemberService` pour actualiser la liste des membres à afficher.
- dans `members-component.html` : 
    - ajouter un champs de recherche
    - liez la champs de recherche à l'attribut `searchPattern` par *two ways binding*
    - lorsqu'un evenement `input` est déclenché sur le champs de recherche, appeler la méthode `onSearchPatternChange` pour actualiser la liste des membres affichés.
