## Utilisation par Anugular d'un web service REST

#### Dans `app.module.ts` :
- importez `HttpClientModule` et ajoutez le à la liste des `ìmports`

#### Dans `members/members.service.ts`
- créez un attribut `url` avec comme valeur "http://localhost:8080/graze-webservice/member"
- créez et injectez un attribut de type `HttpClient`
- modifiez les méthodes pour qu'elles envoient des requètes au web service :
    - `findAll` devient :

            findAll(): Observable<Member[]> {
                return this.hc.get<Member[]>(this.url);
            }

        La valeur renvoyée étant maintenant un observable, pour récupérer la liste des membres vous devez écrire :

            this.ms.findAll().subscribe(lm => // do what you want here with lm);
    - même chose pour les autres méthodes, en suivant les l'architecture de notre web service :

        | URL                   | Http method | Data exchanged | Java service methods    |
        | --------------------- | ----------- | -------------  | -------------------     |
        | member                | GET         | response       | findAll                 |
        | member/id             | GET         | response       | findById                |
        | member/search/pattern | GET         | response       | findByAliasOrEmailLike  |
        | member                | POST        | request        | save                    |
        | member/id             | PUT         | request        | update                  |
        | member/id             | DELETE      | none           | delete                  |

        Notez que la méthode de recherche par alias ou email n'existe pas dans le webservice. Il vous faudra ajouter une méthode à `MemberResource` pour traiter les requètes GET sur l'url `member/search/pattern` (pattern est une variable de chemin). Celle-ci appelera la méthode `findByAliasOrEmailLike` de `MemberService` (à créer) qui elle même appelera `findByAliasOrEmailLike` de `MemberDao` (à créer également).

- supprimez l'attribut `members`

#### Dans les autres composants 
- corrigez les appels des méthodes du service, en appelant `subscribe(...)` sur les observables pour lancer les requètes vers le web service et accéder aux réponses.