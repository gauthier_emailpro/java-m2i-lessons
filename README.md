# Formation 


## Planning

![Planning](Programme.png)


## Supports de cours

### UML
1. [Introduction](Cours/UML/Et3-1Intro.pdf)
2. [Diagramme de cas d'utilisation](Cours/UML/Et3-2CasUtilisation.pdf)
3. [Diagramme de classes](Cours/UML/Et3-3DiagClasses.pdf)
4. [Diagramme de classes (suite)](Cours/UML/Et3-4Operations.pdf)
5. [Diagramme de séquence](Cours/UML/Et3-5DiagSequence.pdf)
6. [Methodologie](Cours/UML/Et3-5SuiteConception.pdf)
7. [Diagramme états-transitions](Cours/UML/Et3-6DiagEtatsTransitions.pdf)

Et la [cheat sheet](Cours/UML/UML-cheat-sheet.pdf) !

### Java SE
1. [Introduction](Cours/Java/Cours/Java - 1 Introduction.pdf)
2. [OOP](Cours/Java/Cours/Java - 2 OOP.pdf)
3. [Generics](Cours/Java/Cours/Java - 3 Generics.pdf)
4. [Collection](Cours/Java/Cours/Java - 4 Collection.pdf)
5. [Exceptions](Cours/Java/Cours/Java - 5 Exceptions.pdf)
6. [Javadoc](Cours/Java/Cours/Java - 7 Javadoc.pdf)
7. [Classes internes et locales](Cours/Java/Cours/Java - 9 Classes internes et locales.pdf)
8. [Java 8](Cours/Java/Cours/Java - 11 Java 8.pdf)
9. [Fichiers](Cours/Java/Cours/Java - 12 Fichiers.pdf)
10. [Log4j](Cours/Java/Cours/Java - 13 - log4j.pdf)

Et la [cheat sheet](Cours/Java/Cours/java-fundamentals.pdf) !

### SQL
1. [Introduction](Cours/SQL/MySQL - cours 1 - Introduction.pdf)
2. [Data manipulation language](Cours/SQL/MySQL - cours 2 - DML.pdf)
3. [Queries](Cours/SQL/MySQL - cours 3 - Queries.pdf)
4. [View](Cours/SQL/MySQL - cours 4 - view.pdf)
5. [Stored procedure & trigger](Cours/SQL/MySQL - cours 5 - stored procedure and trigger.pdf)

### JPA / Hibernate
1. [Cours](Cours/JPA ibernate/jpa.pdf)

Et la [cheat sheet](Cours/JPA Hibernate/jpa-cheat-sheet.pdf) !

### WEB (HTML/CSS/JS)
1. [HTML](Cours/Web/HTML/coursHTML.pdf)
2. [CSS](Cours/Web/CSS/coursCSS.pdf)
3. [JS](Cours/Web/JavaScript/coursJavaScript.pdf)
4. [JS Algorithmique](Cours/Web/JavaScript/coursJSAlgo.pdf)
5. [JS & DOM](Cours/Web/JavaScript/coursJavaScript2.pdf)

### Java EE application web (servlet, JSP, JSTL)
1. [Introduction](Cours/JEE web applications/cours JEE - 1 Introduction.pdf)
2. [Servlet](Cours/JEE web applications/cours JEE - 2 Servlet.pdf)
3. [JSP](Cours/JEE web applications/cours JEE - 3 JSP.pdf)
4. [JSTL](Cours/JEE web applications/cours JEE - 4 JSTL.pdf)
5. [Session](Cours/JEE web applications/cours JEE - 5 Session.pdf)
6. [Form](Cours/JEE web applications/cours JEE - 6 Form.pdf)
7. [Filters](Cours/JEE web applications/cours JEE - 7 Advanced Concept, Filters.pdf)
8. [Ajax](Cours/JEE web applications/cours JEE - 8 Ajax.pdf)

Et la [cheat sheet](Cours/JEE web applications/servlet-cheat-sheet.pdf) !

### Java EE web service (JAX-RS / Jersey)
1. [Cours](Cours/JEE web services/webservice_REST-1.pdf)

### Angular


## Do you want more ?
- `swagger` : un framework pour créer / documenter les webservices
- les threads en Java


# Recherche de stage/job

## Tests techniques
- Test passé par Nicolas lors d'un entretient pour *Interface Technologie* [pdf](Cours/Recherche Emploi/tests/questionnaire - Interface technologie.pdf)

## Recruteurs potentiels
| Entreprise | Contact | Commentaires |
| ---------- |:-------:|:------------ |
| Exemple    | exemple@contact.net | Un exemple pour illustrer la syntaxe. Un autre commentaire. |
